import { isValidMove } from '../game';
import { Game, Move } from '../types';

describe('Tests for isValidMove function', () => {
  // Setup
  test('Should return false if it is an invalid move when he moves to a field marked by the player two', () => {
    const game: Game = {
      moves: [
        { player_one: true, field: 0 },
        { player_one: false, field: 1 },
        { player_one: true, field: 3 },
        { player_one: false, field: 4 },
      ],
      turn_player_one: true,
    };

    const newMove = {
      field: 4,
      player_one: true,
    };

    const expected = false;
    const current = isValidMove(game, newMove);

    expect(current).toBe(expected);
  });

  test('Should return true if it is a valid move', () => {
    const game: Game = {
      moves: [
        { player_one: true, field: 0 },
        { player_one: false, field: 1 },
        { player_one: true, field: 3 },
        { player_one: false, field: 4 },
      ],
      turn_player_one: true,
    };

    const newMove: Move = {
      field: 6,
      player_one: true,
    };

    const expected = true;
    const current = isValidMove(game, newMove);

    expect(current).toBe(expected);
  });

  test('Should return false when the player one try to move in the turn of the player two', () => {
    const game: Game = {
      moves: [
        { player_one: true, field: 0 },
        { player_one: false, field: 1 },
        { player_one: true, field: 3 },
      ],
      turn_player_one: false,
    };

    const newMove: Move = {
      field: 6,
      player_one: true,
    };

    const expected = false;
    const current = isValidMove(game, newMove);

    expect(current).toBe(expected);
  });
});
