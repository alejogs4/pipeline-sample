import { getPossibleWinner } from '../game';
import { Game } from '../types';

describe('Tests for game functions', () => {
  test('Should returns player one as the winner', () => {
    const game: Game = {
      moves: [
        { player_one: true, field: 0 },
        { player_one: true, field: 1 },
        { player_one: true, field: 2 },
        { player_one: false, field: 4 },
        { player_one: false, field: 5 },
      ],
      turn_player_one: false,
    };

    const expected = { winner: 'Player one' };
    const current = getPossibleWinner(game);

    expect(current).toEqual(expected);
  });

  test('Should returns player two as the winner', () => {
    const game: Game = {
      moves: [
        { player_one: true, field: 0 },
        { player_one: true, field: 1 },
        { player_one: false, field: 4 },
        { player_one: false, field: 5 },
        { player_one: false, field: 3 },
      ],
      turn_player_one: false,
    };

    const expected = { winner: 'Player two' };
    const current = getPossibleWinner(game);

    expect(current).toEqual(expected);
  });

  test('Should returns null if any of the players won the game on this move', () => {
    const game: Game = {
      moves: [
        { player_one: true, field: 0 },
        { player_one: true, field: 1 },
        { player_one: false, field: 4 },
        { player_one: false, field: 5 },
      ],
      turn_player_one: false,
    };

    const expected = null;
    const current = getPossibleWinner(game);

    expect(current).toBe(expected);
  });
});
