import { isAWinnerMove } from '../game';

describe('Test for game -> isAWinnerMove function', () => {
  test('Should returns FALSE as it IS NOT a winner move', () => {
    const combination = [3, 8, 7];
    const playerMoves = [1, 2, 4, 6, 8, 5];

    const expected = false;
    const current = isAWinnerMove(combination, playerMoves);

    expect(expected).toEqual(current);
  });

  test('Should returns TRUE as it IS a winner move', () => {
    const combination = [1, 4, 7];
    const playerMoves = [1, 2, 4, 6, 8, 5, 7];

    const expected = true;
    const current = isAWinnerMove(combination, playerMoves);

    expect(expected).toEqual(current);
  });

  test('Should returns a Boolean as the return data', () => {
    const combination = [3, 8, 7];
    const playerMoves = [1, 2, 4, 6, 8, 5];

    const expected = typeof (false);
    const current = typeof (isAWinnerMove(combination, playerMoves));

    expect(expected).toEqual(current);
  });
});
