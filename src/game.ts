/* eslint-disable no-restricted-syntax */
import { Game, Move } from './types';

// Set of fields to win a game
const gameWinCombinations = [
  [0, 1, 2],
  [0, 3, 6],
  [0, 4, 8],
  [3, 4, 5],
  [6, 7, 8],
  [1, 4, 7],
  [2, 5, 8],
  [2, 4, 6],
];

// Verify if the move is valid, with reference to the current game
export const isValidMove = (game: Game, newMove: Move) => (
  game.moves.every((move) => move.field !== newMove.field)
  && game.turn_player_one === newMove.player_one
);

// Verify is with the current move the player win
export const isAWinnerMove = (combination: Array<number>, playerMoves: Array<number>) => (
  combination.every((move) => playerMoves.includes(move))
);

// Get the winner of a game
export const getPossibleWinner = (game: Game) => {
  const movesPlayerOne = game.moves.filter((move) => move.player_one).map((move) => move.field);
  const movesPlayerTwo = game.moves.filter((move) => !move.player_one).map((move) => move.field);
  // Review every combination searching one winner
  for (const combination of gameWinCombinations) {
    if (isAWinnerMove(combination, movesPlayerOne)) return { winner: 'Player one' };
    if (isAWinnerMove(combination, movesPlayerTwo)) return { winner: 'Player two' };
  }

  return null;
};
