export interface Move {
  field: number;
  player_one: boolean;
}

export interface Game {
  moves: Array<Move>;
  turn_player_one: boolean;
}
