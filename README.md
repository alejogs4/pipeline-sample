# Tic Tac Toe - Juego

El archivo game.js contiene funciones que representan el conocido juego "Triki" o "Tic tac toe".

Se representa el juego con una matriz que representa el tablero y los movimientos que van realizando los jugadores. Estos movimientos se validan en cada turno del jugador; se valida si es un movimiento válido y si es el movimiento ganador que definirá la partida.
